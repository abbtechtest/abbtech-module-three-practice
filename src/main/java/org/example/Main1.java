package org.example;

import java.util.Scanner;

public class Main1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Maşının 1k-ə görə yandırdığı benzinin dəyəri: ");
        double benzinDeyeri = scanner.nextDouble();

        System.out.print("Maşının bakındakı hazırki benzin miqdarı: ");
        double benzinMiqdarı = scanner.nextDouble();

        System.out.print("Neçə km məsafə getmək istəyirsiniz?: ");
        double mesafe = scanner.nextDouble();

        if (maşınGedər(benzinDeyeri, benzinMiqdarı, mesafe)) {
            System.out.println("Maşın bu məsafəni gedə bilər.");
        } else {
            System.out.println("Maşın bu məsafəni gedə bilməz.");
        }
    }

    public static boolean maşınGedər(double benzinDeyeri, double benzinMiqdarı, double mesafe) {
        double yolUzunluğu = benzinMiqdarı * 100 / benzinDeyeri; // Maşının gide biləcəyi maksimum məsafə
        return yolUzunluğu >= mesafe; // Maşının məsafəni gedib gedə bilməsi
    }

}