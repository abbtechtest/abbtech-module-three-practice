package org.example;

public class CustomException extends ArrayIndexOutOfBoundsException{
    public CustomException(String message) {
        super(message);
    }
}
